const path = require('path');

module.exports = {
  entry: {
    content_script: path.resolve(__dirname, 'content_scripts/index.ts'),
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js'
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      { test: /\.tsx?$/, loader: "ts-loader" }
    ]
  },
  optimization: {
    minimize: false,
  },
  // Make sure that the output does not include eval()s (used for source maps),
  // as the WebExtension Content Security Policy forbids those:
  devtool: process.env.NODE_ENV !== 'production' ? 'hidden-source-map' : undefined,
};
