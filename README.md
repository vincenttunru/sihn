# Subtle Improvements for Hacker News

This extension aims to make only small uncontroversial tweaks to the Hacker News interface. In other words: it does not introduce a completely different interface, and is not that opinionated.

Many of them are especially useful when browsing on your mobile phone - you'll need [Firefox for Android](https://firefox.com/android) to be able to install the extension.

A list of included tweaks that will without a doubt get out of date:

- Make it easier on touch screens to tap the "collapse" buttons next to comments.
- Make it easier on touch screens to tap the voting buttons next to comments.
- Add links to comments' parent
- Add links to comments' root comment
