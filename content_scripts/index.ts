// Unfortunately, the polyfill cannot be auto-injected with ProvidePlugin, so we manually import it:
// https://github.com/mozilla/webextension-polyfill/issues/156
import browser from 'webextension-polyfill';
import { decorateComments } from './decorateComments';
import { removeAmp } from './amp';

decorateComments();
removeAmp();
