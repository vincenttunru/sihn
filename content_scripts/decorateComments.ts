interface Comment {
  id: string;
  row: HTMLTableRowElement;
  depth: number;
  parents: Comment[];
};

export function decorateComments() {
  const comments = commentsToObjects();
  comments.forEach(addLinkToRoot);
  comments.forEach(addLinkToParent);
}

export function commentsToObjects(): Comment[] {
  if (document.location.pathname !== '/item') {
    return [];
  }
  const commentRows = Array.from(document.querySelectorAll<HTMLTableRowElement>('table.comment-tree > tbody > tr'));
  const lastByDepth: {
    [depth: number]: Comment;
  } = {};
  const comments = commentRows.map((row) => {
    const indentationElement = row.querySelector('.ind img');
    const indentation = (indentationElement)
      ? Number.parseInt(indentationElement.getAttribute('width') || '-1', 10)
      : -40;
    const depth = indentation / 40;
    const parents: Comment[] = [];
    for (let i = 0; i < depth; i++) {
      parents.push(lastByDepth[i]);
    }
    const commentObject: Comment = {
      id: row.getAttribute('id') || 'No ID found on this row',
      row: row,
      depth: depth,
      parents: parents,
    };
    lastByDepth[depth] = commentObject;
    return commentObject;
  });
  return comments;
}
export function addLinkToRoot(comment: Comment) {
  if (comment.depth === 0) {
    return;
  }
  const root = comment.parents[0];
  const replyBox = comment.row.querySelector('.reply p');
  if (!replyBox) {
    return;
  }
  const spacing = document.createTextNode(' ');
  const font = document.createElement('font');
  font.setAttribute('size', '1');
  const underline = document.createElement('u');
  const link = document.createElement('a');
  link.href = `#${root.id}`;
  link.textContent = 'root';
  underline.appendChild(link);
  font.appendChild(underline);
  replyBox.appendChild(spacing);
  replyBox.appendChild(font);
}
export function addLinkToParent(comment: Comment) {
  if (comment.depth < 2) {
    return;
  }
  const parent = comment.parents[comment.depth - 1];
  const replyBox = comment.row.querySelector('.reply p');
  if (!replyBox) {
    return;
  }
  const spacing = document.createTextNode(' ');
  const font = document.createElement('font');
  font.setAttribute('size', '1');
  const underline = document.createElement('u');
  const link = document.createElement('a');
  link.href = `#${parent.id}`;
  link.textContent = 'parent';
  underline.appendChild(link);
  font.appendChild(underline);
  replyBox.appendChild(spacing);
  replyBox.appendChild(font);
}
