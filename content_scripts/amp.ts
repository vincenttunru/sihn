export function removeAmp() {
  if (document.location.pathname !== '/item') {
    return [];
  }

  const links = Array.from(document.getElementsByTagName('a'))
    .filter(anchor => isAmpLink(anchor.href));

  links.forEach(link => {
    link.href = stripAmp(link.href);
    if (link.textContent) {
      link.textContent = stripAmp(link.textContent);
    }
  });
}

export const ampPrefixes = [
  'https://www.google.com/amp/s/',
  'https://www.google.co.uk/amp/s/',
  'https://www.google.nl/amp/s/',
];

export function isAmpLink(link: string) {
  return ampPrefixes.findIndex(prefix => link.substring(0, prefix.length) === prefix) !== -1;
}

export function stripAmp(link: string) {
  link = ampPrefixes.reduce(
    (link, prefix) => {
      if (link.substring(0, prefix.length) === prefix) {
        return 'https://' + link.substring(prefix.length);
      }
      return link;
    },
    link,
  );

  return link;
}
